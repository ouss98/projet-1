package sample;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.JOptionPane;

import connectionDB.DbConnection;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;

public class ControllerTutAdd {
	
	//Qu'on rappel les composante de notre interface graphique dans le Controller
	@FXML private TextField idTuteur;
	@FXML private TextField LastName;
	@FXML private TextField FirstName;
	@FXML private TextField Subject;
	@FXML private TextField passwd;
	@FXML private Button Validate;
	
	 //La m�thode qui rentra en action quand on appuera sur le bouton Valider
	 public void Validate(final ActionEvent event) throws SQLException {
		 
		 Add_tuteur();
        
    }
	 
	 //M�thode pour ajouter un Responsable dans la table responsable de notre base de donn�e
	 public void Add_tuteur () throws SQLException{    


	        //Creation de la connexion à la base de données
	        DbConnection connectionDb = new DbConnection();
	        Connection connection = connectionDb.getConnection();
			
			//Pour ajouter un utilisateur dans la table tuteur de la BDD
	        try {
				String sql = "INSERT INTO tuteur VALUES(?,?,?,?,?)";
				PreparedStatement pst = connection.prepareStatement(sql);
				pst.setString(1, idTuteur.getText());
				pst.setString(2, LastName.getText());
				pst.setString(3, FirstName.getText());
				pst.setString(5, Subject.getText());
				pst.setString(4, passwd.getText());
				pst.executeUpdate();

				JOptionPane.showMessageDialog(null, "", "Users Add succes", 0);
			} catch (Exception e) {
				JOptionPane.showMessageDialog(null, e);
			}


			
		}

}
