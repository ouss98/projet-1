package sample;

import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.ResourceBundle;

import connectionDB.DbConnection;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import mode.Tuteur;

public class ControllerTutList implements Initializable {

	@FXML private TableView<Tuteur> Table;
	@FXML private TableColumn<Tuteur,String> idT;
	@FXML private TableColumn<Tuteur,String> nomT;
	@FXML private TableColumn<Tuteur,String> prenomT;
	@FXML private TableColumn<Tuteur,String> matiereT;
	@FXML private Button AddButton;
	@FXML private Button EditButton;
	@FXML private Button DeleteButton;
	public ObservableList<Tuteur> data = FXCollections.observableArrayList();
	
	@FXML
	private void List() throws SQLException{
		 DbConnection connectionDb = new DbConnection();
	        Connection connection = connectionDb.getConnection();	
		try {
			String sql = "SELECT * FROM tuteur";
			PreparedStatement ps = connection.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			while(rs.next()) {
				data.add(new Tuteur (rs.getString(1),rs.getString(2),rs.getString(3),rs.getString(4)));

			}
			connection.close();
		}catch(Exception e){
			e.printStackTrace();
		}
		idT.setCellValueFactory(new PropertyValueFactory<Tuteur, String>("id"));
		nomT.setCellValueFactory(new PropertyValueFactory<Tuteur, String>("nom"));
		prenomT.setCellValueFactory(new PropertyValueFactory<Tuteur, String>("prenom"));
		matiereT.setCellValueFactory(new PropertyValueFactory<Tuteur, String>("matiere"));
		Table.setItems(data);
	}

	public void Add(final ActionEvent event) throws SQLException {
		event.consume();

		// if(isAdmin()) {
		try {
			GridPane page = (GridPane) FXMLLoader.load(getClass().getResource("sampleTutAdd.fxml"));
			Scene newScene = new Scene(page);
			Stage stage = new Stage();
			stage.setScene(newScene);
			stage.show();
			//	Page_Sel_SA.start();

		} catch (Exception e) {
			e.printStackTrace();
		}

	}
	public void Edit(final ActionEvent event) throws SQLException {
		event.consume();

		// if(isAdmin()) {
		try {
			GridPane page = (GridPane) FXMLLoader.load(getClass().getResource("sampleTutEdit.fxml"));
			Scene newScene = new Scene(page);
			Stage stage = new Stage();
			stage.setScene(newScene);
			stage.show();
			//	Page_Sel_SA.start();

		} catch (Exception e) {
			e.printStackTrace();
		}

	}
	public void Delete(final ActionEvent event) throws SQLException {
		event.consume();

		// if(isAdmin()) {
		try {
			GridPane page = (GridPane) FXMLLoader.load(getClass().getResource("sampleTutDelete.fxml"));
			Scene newScene = new Scene(page);
			Stage stage = new Stage();
			stage.setScene(newScene);
			stage.show();
			//	Page_Sel_SA.start();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		// TODO Auto-generated method stub
		
	}
}
/*public void Add_tuteur (){    

		String usernameDb = "";
		String passwordDb = "";

		//Creation de la connexion � la base de donn�es
		DbConnection connectionDb = new DbConnection();
		Connection connection = connectionDb.getConnection();


		String sql = "insert into tuteur (idTuteur,nom,prenom,matiere)values(?,?,?,? )";
		try {
			pst = connection.prepareStatement(sql);
			pst.setString(1, txt_idTuteur.getText());
			pst.setString(2, txt_nom.getText());
			pst.setString(3, txt_prenom.getText());
			pst.setString(4, txt_matiere.getText());
			pst.execute();
			UpdateTable();
			search_user();

			JOptionPane.showMessageDialog(null, "Users Add succes");
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, e);
		}
	}

	public void Edit (){   
		try {
			conn = mysqlconnect.ConnectDb();
			String value1 = txt_idTuteur.getText();
			String value2 = txt_nom.getText();
			String value3 = txt_prenom.getText();
			String value4 = txt_matiere.getText();
			String value5 = txt_idEtud.getText();
			String sql = "update tuteur set idTuteur= '"+value1+"',nom= '"+value2+"',prenom= '"+
					value3+"',matiere= '"+value4+"',idEtud= '"+value5+"' where idTuteur='"+value1+"' ";
			pst= conn.prepareStatement(sql);
			pst.execute();
			JOptionPane.showMessageDialog(null, "Update");
			UpdateTable();
			search_user();
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, e);
		}

	}
/*
	public void Delete(){
		conn = mysqlconnect.ConnectDb();
		String sql = "delete from tuteur where idTuteur = ?";
		try {
			pst = conn.prepareStatement(sql);
			pst.setString(1, txt_id.getText());
			pst.execute();
			JOptionPane.showMessageDialog(null, "Delete");
			UpdateTable();
			search_user();
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, e);
		}

	}*/
