package sample;

import connectionDB.DbConnection;
import javafx.fxml.FXMLLoader;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;


public class ControllerConnection {
	
	

    //Username et password récupérés de la saisie sur le FXML
    @FXML private TextField usernameField;
    @FXML private TextField passwordField;

    
    // Pour passer a la page suivante
    @FXML
    private void connectAdminPage(ActionEvent event) throws SQLException {
       event.consume();

        if(isAdmin()) {
            try {
               GridPane page = (GridPane) FXMLLoader.load(getClass().getResource("sampleAdmin.fxml"));
                Scene newScene = new Scene(page);
                Stage stage = new Stage();
                stage.setScene(newScene);
                stage.show();   
            } catch (Exception e) {
                e.printStackTrace();
            }
            
            // Afin de fermer la page pr�c�dente
            final Node source = (Node) event.getSource(); 
            final Stage stage = (Stage) source.getScene().getWindow(); 
            stage.close(); 
            
           
        }

    }

    //Ici une methode pour v�rifier si Les identifiants sont bon
    private boolean isAdmin() throws SQLException {
        boolean isAdmin = false;
        String usernameDb = "";
        String passwordDb = "";

        //Creation de la connexion à la base de données
        DbConnection connectionDb = new DbConnection();
        Connection connection = connectionDb.getConnection();

        //Selection en bdd des utilisateurs
        String sql = "SELECT idAd, mdp FROM admin";
        Statement statement = connection.createStatement();

        //Resultat de la requete
        ResultSet resultSet = statement.executeQuery(sql);
        while(resultSet.next()){
            usernameDb = resultSet.getString(1);
            passwordDb = resultSet.getString(2);
   
        }
        //Si les username/mdp correspondent au user admin
        if( usernameField.getText().equals(usernameDb) && passwordField.getText().equals(passwordDb)){
            isAdmin = true;
        }
        else{
            isAdmin = false;
        } 
        return isAdmin;
    }
    
    
    public boolean IsItOk() {
    	String usernameDb = "";
        String passwordDb = "";

        //Creation de la connexion à la base de données
        DbConnection connectionDb = new DbConnection();
        Connection connection = connectionDb.getConnection();

        //Selection en bdd des utilisateurs
        String sql = "SELECT idAd, mdp FROM admin";
        String a = usernameField.getText();
        String b = passwordField.getText();
        String c = a + b;
        
        if (c.compareTo(sql) == 0) {
	     return true;
        }
        else {
	     return false;
        }
    }
    

}
