package connectionDB;

import java.sql.Connection;
import java.sql.DriverManager;

public class DbConnection {
    public Connection connection;
    public Connection getConnection(){
        String nomDb="projet";
        String utilisateur="root";
        String mdp="";

        try{
            Class.forName("com.mysql.cj.jdbc.Driver");

            connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/projet1?user=root&useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC");
        } catch(Exception e){
            e.printStackTrace();
        }

        return connection;
    }
}
