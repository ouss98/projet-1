package mode;

public class Tuteur {
	private String id;
	private String nom;
	private String prenom;
	private String matiere;

	
	public Tuteur() {
		super();
	}
	public Tuteur(String id, String nom, String prenom, String matiere) {
		super();
		this.id=id;
		this.nom=nom;
		this.prenom=prenom;
		this.matiere=matiere;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getPrenom() {
		return prenom;
	}
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
	public String getMatiere() {
		return matiere;
	}
	public void setMatiere(String matiere) {
		this.matiere = matiere;
	}

	
}
